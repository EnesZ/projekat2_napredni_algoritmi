#include <iostream>
#include <time.h>
#include <random>
#include "sat_utils.h"

using namespace std;

int main()
{

    generate_clauses(V_NUM,C_NUM,c_list);
    generate_variables(c_list,v_list,V_NUM);
    vector<int> v_state1;
    vector<int> v_state2;
    vector<int> v_state3;

    local_search(c_list,v_list,var_state);
    local_search_smart(c_list,v_list,var_state);
    tabu_sat(c_list,v_list,v_state1);
    sa_sat(c_list,v_list,v_state2);

    /*
    for(int i=0;i<c_list.size();i++){
        for(int j=0;j<c_list.at(i).size();j++)
            cout<<c_list.at(i).at(j)<<" ";
        cout<<endl;
    }
    cout<<"........."<<endl;
    for(int i=0;i<v_list.size();i++){
        for(int j=0;j<v_list.at(i).size();j++)
            cout<<v_list.at(i).at(j)<<" ";
        cout<<endl;
    }
    cout<<is_sat_true(c_list);*/
    return 0;
}
