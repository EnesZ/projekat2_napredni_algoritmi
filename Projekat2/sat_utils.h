#ifndef SAT_UTILS_H
#define SAT_UTILS_H
#include <vector>
#include <time.h>
#include <random>
#include <math.h>

using namespace std;


//number of clauses
int C_NUM = 500;
//number of variables
int V_NUM = 50;
int MAX_TRIES = 1000000;
float MAX_TEMP = 0.3;
float MIN_TEMP = 0.01;




vector<vector<int>> c_list;
vector<vector<int>> v_list;
vector<int> var_state;


// c_list = [ [0, v_k1, v_k2, v_k3], [0, v_k4, v_k5, v_k6] ...]
// [number of true literals, -2, -3, 4(variables in the clausule)]
void generate_clauses(int v_num,int c_num, vector<vector<int>> &c_list){
    srand(time(0));

    for(int i=0;i<c_num;i++){
        vector<int> clausule;
        clausule.push_back(0);
        for(int i=0;i<3;i++){
            int random_var;
            random_var = rand() % v_num + 1;
            if(rand()%2==0)
                random_var*=(-1);
            clausule.push_back(random_var);
        }
        c_list.push_back(clausule);
    }

}

// v_list= [ [index clauses that variable v1 is, clause1, clause2,..],
//           [index clauses that variable v2 is, clause1, clause2,..] ]


void generate_variables(const vector<vector<int>> &c_list, vector<vector<int>> &v_list, int v_num){
    //initialization
    for(int i=0;i<v_num;i++){
        vector<int> variable;
        variable.push_back(0);
        v_list.push_back(variable);
    }


    for(int i=0;i<c_list.size();i++){

        for(int j=1;j < c_list.at(i).size();j++){

            int var_temp = c_list.at(i).at(j);
            int neg = 1;
            if(var_temp<0)
                neg*=(-1);

            v_list.at(abs(var_temp)-1).push_back((i+1)*neg);


            v_list.at(abs(var_temp)-1).at(0)++;
        }
    }
}

void generate_state(vector<int>& var_state, int v_num,vector<vector<int>> &c_list){
    for(int i=0;i<v_num;i++){

        if(rand()%2==0)//negative
            var_state.push_back(-1);
        else
            var_state.push_back(1);
    }
    //update c_list for number of true literals
    for(int i=0;i<c_list.size();i++){
        c_list.at(i).at(0)=0;

        for(int j=1;j<c_list.at(i).size();j++){
            int var = c_list.at(i).at(j);
            if(var*var_state.at(abs(var)-1)>0){
                c_list.at(i).at(0)++;

            }
        }

    }


}

bool is_sat_true(const vector<vector<int>>& c_list){
    for(int i=0;i<c_list.size();i++)
        if(c_list.at(i).at(0)<= 0)
            return false;

    return true;
}

int compute_cost(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ,int v){
    int delta = 0;

        for(int j=1;j<v_list.at(v).size();j++){
            int clause = v_list.at(v).at(j);
            if(clause * var_state.at(v)>0){
                //zamjenom bi smanjili broj poz literala
                //zamjenom bi eventualno mogli uticati na tacnost klauzule
                if(c_list.at(abs(clause)-1).at(0)==1){
                    //ako u klauzuli postoji tacno 1 literal
                    //klauzula postaje netacna
                    delta--;
                }
            }else{
                //zamjenom povecavamo broj pozitivnih literala
                //zamjenom bi eventualno klauzula mogla postati pozitivna
                if(c_list.at(abs(clause)-1).at(0)==0){
                    //ako u klauzuli postoji 0 tacnih literala
                    //klauzula postaje tacna
                    delta++;
                }
            }

        }

    return delta;

}
float sigmoid(float delta,float temperature){

    return 1/(1+exp(delta/temperature));

}
int do_flip(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ,int v){

    for(int i=1;i<v_list.at(v).size();i++){
        int clause_idx = abs(v_list.at(v).at(i))-1;
        if(v_list.at(v).at(i) * var_state.at(v)>0){
            c_list.at(clause_idx).at(0)--;
        }else
            c_list.at(clause_idx).at(0)++;

    }
    //flip
    var_state.at(v)*=(-1);
}

int positive_clauses(vector<vector<int>>& c_list){

    int s = 0;
    for(int i=0;i<c_list.size();i++)
        if(c_list.at(i).at(0)>0)
            s++;
    return s;
}
bool sa_sat(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ){


    cout<<"simultano"<<endl;

    int i=0;
    int tries = 0;
    int flips = 0;
    int max_pos_c = 0;

    while(true){
        i++;
        generate_state(var_state,V_NUM,c_list);
        int j=0;
        while(true){
            if(is_sat_true(c_list)){
                cout<<"Number of tries "<<tries<<endl<<" Numbers of flips "<<flips;
                return true;
            }
            float decay_rate = 1/(i*V_NUM);
            float temperature = MAX_TEMP * exp((-1)*j*decay_rate);
            if(temperature<MIN_TEMP)
                break;

            for(int v=0;v<V_NUM;v++){

                if(v_list.at(v).at(0)==0)
                    continue;//we dont have this variable in clauses

                //compute cost
                //Compute the increase (decrease) delta in the number of clauses made true if v were flipped

                int delta = compute_cost(c_list,v_list,var_state,v);

                //flip variable v with probability defined by the
                float prob = sigmoid(delta,temperature);


                if(rand()/RAND_MAX<(1-prob)){
                    //flip v
                    do_flip(c_list,v_list,var_state,v);

                    int c_temp = positive_clauses(c_list);
                    if(max_pos_c<c_temp)
                        max_pos_c = c_temp;


                    flips++;
                    if(is_sat_true(c_list)){
                        cout<<"Number of tries "<<tries<<endl<<" Numbers of flips "<<flips;
                        return true;
                    }
                }

            }

            j++;
            tries++;
            if(tries%10000==0)
                cout<<tries/1000<<"k, max clauses:"<<max_pos_c<<" cur c "<<positive_clauses(c_list)<<"/"<<C_NUM<<endl;
            if(MAX_TRIES <=tries)
                return false;
        }
    }
}

bool check_states(vector<vector<int>>& states, int v){

    vector<int> last_state = states.back();
    vector<int> new_state = last_state;
    new_state.at(v)*=(-1);


    for(int i=0;i<states.size();i++){
        bool same=true;
        for(int j=0;j<states.at(i).size();j++){

            if(states.at(i).at(j)!=new_state.at(j)){
                same = false;
                break;

            }

        }
        if(same)
            return false;

    }
    return true;
}



bool tabu_sat(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ){

    cout<<"tabu"<<endl;
    vector<vector<int>> states;

    int i=0;
    int tries = 0;
    int flips = 0;
    int max_pos_c = 0;
    while(true){
        i++;
        vector<vector<int>> states;
        generate_state(var_state,V_NUM,c_list);
        states.push_back(var_state);
        int j=0;
        while(true){
            if(is_sat_true(c_list)){
                cout<<"Number of tries "<<tries<<endl<<" Numbers of flips "<<flips;
                return true;
            }
            float decay_rate = 1/(i*V_NUM);
            float temperature = MAX_TEMP * exp((-1)*j*decay_rate);
            if(temperature<MIN_TEMP)
                break;

            for(int v=0;v<V_NUM;v++){

                if(v_list.at(v).at(0)==0)
                    continue;//we dont have this variable in clauses

                if(check_states(states, v)==false)
                    continue; //we already visited this state in the last n states

                //compute cost
                //Compute the increase (decrease) delta in the number of clauses made true if v were flipped

                int delta = compute_cost(c_list,v_list,var_state,v);

                //flip variable v with probability defined by the
                float prob = sigmoid(delta,temperature);

                if(rand()/RAND_MAX<(1-prob)){
                    //flip v

                        do_flip(c_list,v_list,var_state,v);

                        int c_temp = positive_clauses(c_list);
                        if(max_pos_c<c_temp)
                            max_pos_c = c_temp;

                        flips++;
                        states.push_back(var_state);
                        if(states.size()>10)
                            states.erase(states.begin());

                    if(is_sat_true(c_list)){
                        cout<<"Number of tries "<<tries<<endl<<" Numbers of flips "<<flips<<endl;
                        return true;
                    }
                }

            }

            j++;
            tries++;
            if(tries%1000==0)
                cout<<tries/1000<<"k, max clauses:"<<max_pos_c<<" cur c "<<positive_clauses(c_list)<<"/"<<C_NUM<<endl;
            if(MAX_TRIES/10 <=tries)
                return false;
        }
    }





}

int MAX_FLIPS = 100;
bool local_search_smart(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ){

    cout<<"smart"<<endl;
    for(int i=0;i<MAX_TRIES/MAX_FLIPS;i++){

        generate_state(var_state,V_NUM,c_list);
        for(int j=0;j<MAX_FLIPS;j++){

            if(is_sat_true(c_list)){
                cout<<"Number of tries "<<i<<endl<<" Numbers of flips "<<i*j<<endl;
                return true;
            }

            int delta = (-1)*RAND_MAX;
            int v2flip = -1;
            for(int v=0;v<V_NUM;v++){

                if(v_list.at(v).at(0)==0)
                        continue;//we dont have this variable in clauses

                int delta_temp = compute_cost(c_list,v_list,var_state,v);
                if(delta_temp > delta){
                    delta = delta_temp;
                    v2flip = v;
                }

            }
            //flip variable with the max delta
            do_flip(c_list,v_list,var_state,v2flip);

        }
        if(i%1000==0)
                cout<<i*MAX_FLIPS/1000<<"k, positive clauses:"<<positive_clauses(c_list)<<"/"<<C_NUM<<endl;
    }

    return false;
}

bool local_search(vector<vector<int>>& c_list,vector<vector<int>>& v_list,vector<int>& var_state ){

    cout<<"greedy"<<endl;
    for(int i=0;i<MAX_TRIES/MAX_FLIPS;i++){

        generate_state(var_state,V_NUM,c_list);
        for(int j=0;j<MAX_FLIPS;j++){

            if(is_sat_true(c_list)){
                cout<<"Number of tries "<<i<<endl<<" Numbers of flips "<<i*j<<endl;
                return true;
            }

            int random_var = rand() % V_NUM;
            //flip random variable
            int delta_temp = compute_cost(c_list,v_list,var_state,random_var);
            if(delta_temp>0)
                do_flip(c_list,v_list,var_state,random_var);

        }
        if(i%1000==0)
                cout<<i*MAX_FLIPS/1000<<"k, positive clauses:"<<positive_clauses(c_list)<<"/"<<C_NUM<<endl;
    }

    return false;
}

#endif // SAT_UTILS_H
